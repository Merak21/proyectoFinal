Nombre: Ramos Calpulalpan Karem.

Número de cuenta: 314068583.

Justificación: Usamos un estructura de árbol AVL, para evitar que el tiempo 
sea líneal, y sea O(nlongn).

Versión de NetBeans: 8.1.

Ruta relativa: src/karemramoscalpulalpan_proyectofinal/objetosDeEmpaquetamiento.txt 

Descripción de formato del archivo de entrada: El tamaño del contenedor es el
mismo, así que eso se define con un número natural en el primer renglón del 
archivo, en el segundo renglón encontramos el id del objeto, nombre del objeto,
el tamaño del objeto y una pequeña descripción del objeto, todo separado por ";".
Cada renglón después del pirmero, tiene la misma estructura.

Descripción de formato del archivo de salida: karemRamosCalpulalpan_ProyectoFinal/

