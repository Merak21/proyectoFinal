package karemramoscalpulalpan_proyectofinal;

/**
 * Similaremos la estructura de un arból AVL.
 *
 * @author Karem Ramos Calpulalpan.
 * @version 1. Miércoles 13 de junio, 2018.
 */
public class ArbolAVL {

    /*Atributos de clase.*/
    private NodoAVL raiz;

    /**
     * Constructor vacío.
     */
    public ArbolAVL(){
    } //Fin del método constructor vacío.

    /**
     * Instanciamos los atributos.
     * 
     * @param raiz, del árbol.
     */
    public ArbolAVL(NodoAVL raiz){
	raiz = null;
    } //Fin del método constructor.
    
    /**
     * Obtine la raíz del árbol.
     *
     * @return la raíz, del árbol.
     */
    public NodoAVL obtenerRaiz(){
        return raiz;
    } //Fin de obtenerRaiz.

    /**
     * Obtiene el factor de equilibrio del nodo.
     *
     * @return factor de equilibrio, del nodo.
     */
    public int obtenFactorEqui(NodoAVL nodo){
	if (nodo == null) {
            return -1;
        } else {
	return nodo.factorEqui;
	}
    } //Fin de obtenerFactorEqui

    /**
     * Hace la rotación simple izquierda
     *
     * @param NodoAVL.
     * @return nodo auxiliar.
     */
    public NodoAVL hacerRotacionIzquierda(NodoAVL nodo){
        NodoAVL auxiliar = nodo.hijoIzquierdo;
        nodo.hijoIzquierdo = auxiliar.hijoDerecho;
        auxiliar.hijoDerecho = nodo;
        nodo.factorEqui = Math.max(obtenFactorEqui(nodo.hijoIzquierdo), obtenFactorEqui(nodo.hijoDerecho))+1;
        auxiliar.factorEqui = Math.max(obtenFactorEqui(auxiliar.hijoIzquierdo), obtenFactorEqui(auxiliar.hijoDerecho))+1;
        return auxiliar;
    } //Fin de hacerRotacionIzquierda.
    
    /**
     * Hace la rotación simple derecha.
     *
     * @param nodo, del árbol AVL.
     * @return nodo auxiliar.
     */
    public NodoAVL hacerRotacionDerecha(NodoAVL nodo){
        NodoAVL auxiliar = nodo.hijoDerecho;
        nodo.hijoDerecho = auxiliar.hijoIzquierdo;
        auxiliar.hijoIzquierdo = nodo;
        nodo.factorEqui = Math.max(obtenFactorEqui(nodo.hijoIzquierdo), obtenFactorEqui(nodo.hijoDerecho))+1;
        auxiliar.factorEqui = Math.max(obtenFactorEqui(auxiliar.hijoIzquierdo), obtenFactorEqui(auxiliar.hijoDerecho))+1;
        return auxiliar;
    } // Fin de hacerRotaciónDerecha.
    
    /**
     * Hace la rotación doble izquierda.
     *
     * @param nodo, del árbolAVL.
     * @return nodo auxiliar.
     */
    public NodoAVL hacerRotacionDobleIzquierda(NodoAVL nodo){
        NodoAVL temporal;
        nodo.hijoIzquierdo = hacerRotacionDerecha(nodo.hijoIzquierdo);
        temporal = hacerRotacionIzquierda(nodo);
        return temporal;
    } //Fin de hacerRotacionDobleIzquierda.

    /**
     * Hace una rotación doble a la dechera.
     *
     * @param nodo, del árbolAVL.
     */
    public NodoAVL hacerRotacionDobleDerecha(NodoAVL nodo){
        NodoAVL temporal;
        nodo.hijoDerecho = hacerRotacionIzquierda(nodo.hijoDerecho);
        temporal = hacerRotacionDerecha(nodo);
        return temporal;
    } //Fin de haceRotacionDobleDerecha.
    
    /**
     * Inserta un nodo entero al árbol.
     *
     * @param nodo nuevo
     * @param nodo x
     * @return el nuevo padre.
     */
    public NodoAVL insertaNodo(NodoAVL nodoNuevo, NodoAVL subArbol){
        NodoAVL nuevoPadre = subArbol;
        if (nodoNuevo.dato < subArbol.dato){
            if (subArbol.hijoIzquierdo == null){
                subArbol.hijoIzquierdo = nodoNuevo;
            } else {
                subArbol.hijoIzquierdo = insertaNodo(nodoNuevo, subArbol.hijoIzquierdo);
                if ((obtenFactorEqui(subArbol.hijoIzquierdo) - obtenFactorEqui(subArbol.hijoDerecho) == 2)){
                    if (nodoNuevo.dato < subArbol.hijoIzquierdo.dato){
                        nuevoPadre = hacerRotacionIzquierda(subArbol);
                    } else {
                        nuevoPadre = hacerRotacionDobleIzquierda(subArbol);
		    } //Fin del 3 else
		} //Fin del if.
            } //FIn del else.
        } else if (nodoNuevo.dato > subArbol.dato){
            if (subArbol.hijoDerecho == null){
                subArbol.hijoDerecho = nodoNuevo;
            } else {
                subArbol.hijoDerecho = insertaNodo(nodoNuevo,subArbol.hijoDerecho);
                if ((obtenFactorEqui(subArbol.hijoDerecho) - obtenFactorEqui(subArbol.hijoIzquierdo) == 2)){
                    if (nodoNuevo.dato > subArbol.hijoDerecho.dato){
                        nuevoPadre = hacerRotacionDerecha(subArbol);
                    } else {
                        nuevoPadre = hacerRotacionDobleDerecha(subArbol);
                    } //Fin del else.
                } //Fin del if.
            } //Fin del else.
        } else {
            System.out.println("Nodo Duplicado");
        } //Fin del else.
	
        /*Actualizamos la altura*/
        if ((subArbol.hijoIzquierdo == null) && (subArbol.hijoDerecho != null)){
            subArbol.factorEqui = subArbol.hijoDerecho.factorEqui + 1;
        } else if ((subArbol.hijoDerecho == null) && (subArbol.hijoIzquierdo != null)){
            subArbol.factorEqui = subArbol.hijoIzquierdo.factorEqui + 1;
        } else {
            subArbol.factorEqui = Math.max(obtenFactorEqui(subArbol.hijoIzquierdo), obtenFactorEqui(subArbol.hijoDerecho)) + 1;
        } //Fin del else
        return nuevoPadre;
    } //Fin de insertaNodo.

    /**
     * Inserta un elemento entero al árbol.
     *
     * @param el elemnto a insertar.
     */
    public void insertaElemento(int dato){
        NodoAVL nuevoElemento = new NodoAVL(dato);
        if (raiz == null){
            raiz = nuevoElemento;
        } else {
            raiz = insertaNodo(nuevoElemento, raiz);
        } //Fin del else.
    } //Fin de InsertaElementos.
        
    /**
     * Muestra los datos en el orden "inOrder"
     *
     * @param el nodo raíz.
     */
    public void muestraInOrder(NodoAVL raiz){
        if (raiz != null){
            muestraInOrder(raiz.hijoIzquierdo);
            System.out.println(raiz.dato + ", ");
            muestraInOrder(raiz.hijoDerecho);
        } //Fin del if.
    } //Fin de inOrder.

    /**
     * Mustra los datos en "preOrder"
     *
     * @param raiz
     */
    public void muestraEnPreOrder(NodoAVL raiz){
        if (raiz != null){
	    System.out.println( raiz.dato + ", ");
            muestraEnPreOrder(raiz.hijoIzquierdo);
            muestraEnPreOrder(raiz.hijoDerecho);
        } //Fin del if.
    } //Fin de preOrder

    /**
     * MUestra los datos en posOrder
     *
     * @param raiz
     */
    public void muestraEnPostOrder(NodoAVL raiz){
        if (raiz != null){
            muestraEnPostOrder(raiz.hijoIzquierdo);
            muestraEnPostOrder(raiz.hijoDerecho);
            System.out.println(raiz.dato + ", ");
        } //Fin del if
    } //Fin de postOrder.

} //Fin de la clase ArbolAVL.
