/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package karemramoscalpulalpan_proyectofinal;

/**
 * Modelamos un contendor básico.
 *
 * @author Karem Ramos Calpulalpan.
 * @version 1. Miércoles 13 de junio,2018.
 */
public class Contenedor {

    /*Atributos de clase*/
    private int tamanio;
    public Objeto objeto;
 
   /*Constructores*/
   /**
    * Constructor que recibe sólo el tamaño.
    * 
    * @param tamaño del contenedor.
    */
    public Contenedor(int tamanio){
        this.tamanio = tamanio;
    } //Fin del método constructor.

    /**
     * Instanciamos los atributos de clase
     * @param tamnio, del contenedor.
     * @param OBjetos, para el contedor.
     */
    public Contenedor(int tamanio, Objeto o){
        this.tamanio = tamanio;
        this.objeto = o;
    } //FIn del método constructor

    /**
     * Obtenemos el tamño del contenedor
     * 
     * @return el tamaño del contenedor.
     */
    public int getTamaño(){
        return tamanio;
    } //Fin de getTamnio

    /**
     * MOdificamos el tamanio del contedor-
     *
     * @param el tamanio, del contendor.
     */
    public void setTamaño(int tamaño){
        this.tamanio = tamanio;
    } //Fin del tamnio del contendor.    

    /**
     * Obtenemos el objeto.
     * 
     * @return el objeto.
     */
    public Objeto getObjeto(){
        return objeto;
    } //Fin getObjeto

    /**
     * Modificamos el objeto.
     *
     * @param el objeto.
     */
    public void setObjeto(Objeto o){
        this.objeto = o;
    } //Fin de setOBjeto.
    
} //Fin de la clase contenedor.

