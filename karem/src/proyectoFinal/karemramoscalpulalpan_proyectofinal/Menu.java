/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package karemramoscalpulalpan_proyectofinal;

/**
 * Muestra las distintas opciones que tiene el usuario.
 *
 *@author Karem Ramos Calpulalpan.
 *@version 1 Sábado 9 de junio, 2018.
 */
public class Menu {

    /**
     * Muestra los servicios que se ofrecen.
     * 
     * @return la cadena de servicios. 
     */
    public String muestraServicios() {
	
	return "****** Bienvenido al servidor. ******\n\n" +
	    "Elige una de las siguiente opciones utilizando " +
	    "números del 0-4, por favor.\n" +
	    "[0] Salir. \n" +
	    "[1] Mostrar objetos a empaquetar.\n" +
	    "[2] Mostrar el tamaño del contenedor leído.\n" +
	    "[3] Hacer empaquetamiento.\n" +
	    "[4] Guardar empaquetamiento.\n";
	
    } //Fin de muestraServicios.
    
    /**
     * Muestra una despedida cordial al usuario.
     * 
     * @return un mensaje cordial.
     */
    public String muestraDespedida() {
	
	return "\nFue un placer servirte. ;) \n" + "Regresa  pronto.\n";
	
    } //Fin de muestraDespedida
    
} //Fin de la clase ServidorAuxiliar.

