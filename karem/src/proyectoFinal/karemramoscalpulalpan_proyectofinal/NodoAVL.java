package karemramoscalpulalpan_proyectofinal;

/**
 * Modelamos el nodo de un árbol AVL.
 *
 * @author Karem Ramos Calpulalpan.
 * @version 1. Miércoles 13 de junio, 2018.
 */
public class NodoAVL {

    /*Atributos de clase*/
    int dato;
    int factorEqui;
    NodoAVL hijoIzquierdo;
    NodoAVL hijoDerecho;

    /**
     * Instanciamos los atributos
     *
     * @param dato, en entero.
     * @param factor de equilibrio, del nodo.
     * @param hijoDerecho.
     * @param hijoIzquierdo.
     */
    public NodoAVL(int dato, int factorEqui, NodoAVL hijoIzquierdo, NodoAVL hidoDerecho){	
        this.dato = dato;
        this.factorEqui = 0;
        this.hijoIzquierdo = null;
        this.hijoDerecho = null;
    } //Fin del método constructor.
    /**
     * Constructor que recibe sólo el dato.
     *
     * @param dato
     */
    public NodoAVL(int dato) {
	this.dato = dato;
	this.factorEqui = 0;
        this.hijoIzquierdo = null;
        this.hijoDerecho = null;
    } //Fin del método constructor.
    
    /**
     * Obtiene el dato del nodo.
     *
     * @return dato, del nodo.
     */
    public int getDato(){
	return dato;
    } //Fin de getDato.

    /**
     * Modifica el dato del nodo.
     *
     * @param dato, del nodo.
     */
    public void setDato(int dato){
	this.dato = dato;
    } //Fin de setDato.

    /**
     * Obtiene el factor de equilibrio del nodo.
     *
     * @return factor de equilibrio, del nodo.
     */
    public int getFactorEqui(){
	return factorEqui;
    } //Fin de getFactorEqui.

    /**
     * Modifica el factor de equilibrio del nodo.
     *
     * @param factor de equilibrio, del nodo.
     */
    public void setFactorEqui(int dato){
	this.factorEqui = factorEqui;
    } //Fin de setFactorEqui.

    /**
     * Obtiene el hijo derecho del nodo.
     *
     * @return el hijo derecho, del nodo.
     */
    public NodoAVL getHijoDerecho(){
	return hijoDerecho;
    } //Fin de getHIjoDerecho.

    /**
     * Modifica el hijo derecho del nodo.
     *
     * @param hijo derecho, del nodo.
     */
    public void setHijoDerecho(NodoAVL hijoDerecho){
	this.hijoDerecho = hijoDerecho;
    } //Fin de setHijoDerecho.

    /**
     * Obtiene el hijo izquierdo del nodo.
     *
     * @return el hijo izquierdo, del nodo.
     */
    public NodoAVL getHijoIzquierdo() {
	return hijoIzquierdo;
    } //Fin de getHijoIzquierdo.

    /**
     * Modifica el hijo izquierdo del nodo.
     *
     * @param hijo izquierdo, del nodo.
     */
    public void setHijoIzquierdo(NodoAVL hijoIzquierdo) {
	this.hijoIzquierdo = hijoIzquierdo;
    } //Fin de setHijoIzquierdo.
    
} //FIn de la clase NodoAVL.

