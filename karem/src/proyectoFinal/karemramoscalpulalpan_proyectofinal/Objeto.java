/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package karemramoscalpulalpan_proyectofinal;

/**
 * Obtiene lo necesario de un objeto.
 * 
 * @author Karem Ramos Calpulalpan.
 * @version 1 Miércoles 13 de junio, 2018.
 */
public class Objeto {

    /*Atributos de clase*/
    private String id;
    private String nombre;
    private int tamanio;
    private String descripcion;

    /**
     * Instaciamos los atributos.
     * @param id 
     * @param nombre
     * @param tamanio
     * @param descripcion
     */
    public Objeto(String id, String nombre, int tamanio, String descripcion){
        this.id = id;
        this.nombre = nombre;
        this.tamanio = tamanio;
        this.descripcion = descripcion;
    } //Fin del método constructor

    /*
     * Constructor sin parámetros
     */
    public Objeto(){
    } //Fin de método constructor sin parámetros.

    /**
     * Obtiene el id, del objeto.
     *
     * @return el id, del objeto.
     */ 
    public String getId(){    
        return id;
    } //Fin de getId.

    /**
     * Modifica el id del objeto.
     *
     * @param id, del objeto.
     */
    public void setId(String id){
        this.id = id;
    } //Fin de setId.
    
    /**
     * Obtiene el nombre del objeto.
     * 
     * @return nombre, del objeto.
     */
    public String getNombre(){
        return nombre;
    } //Fin de getNombre.

    /**
     * Modifica el nombre del objeto.
     * 
     * @param el nombre, del objeto.
     */
    public void setNombre(String nombre){
        this.nombre = nombre;
    } //Fin del setNombre.

    /**
     * Obtiene el tamaño del objeto.
     *
     * @return el tamaño, del objeto.
     */
    public int getTamanio(){
        return tamanio;
    } //Fin de getTamanio

    /**
     * Modifica el tamaño del objeto,
     *
     * @param el tamaño del objeto
     */
    public void setTamanio(int tamanio){
        this.tamanio = tamanio;
    } //Fin de setTamnio.
    
    /**
     * Obtiene la descripción del objeto.
     *
     * @return la descripción, del objeto.
     */
    public String getDescripcion(){
        return descripcion;
    } //Fin de getDescripcion.

    /**
     * Modifica la descripcion del objeto
     *
     * @param la descripción, del objeto.
     */
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    } //Fin de setDescripcion.
     
} //Fin de la clase Objeto.

