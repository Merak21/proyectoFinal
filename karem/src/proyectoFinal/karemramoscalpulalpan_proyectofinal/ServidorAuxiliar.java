/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package karemramoscalpulalpan_proyectofinal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Crea un archivo txt, con nombre especifíco.
 *
 * @author Karem Ramos Calpulalpan
 */
public class ServidorAuxiliar {

    //ServidorAuxiliar servicio = new ServidorAuxiliar();

    /**
     * 
     * @param nombre, que tendrá el archivo de texto.
     * @param texto, los datos que guardará. 
     */
    public void creaTxt(String texto) {
        File archivo;
        FileWriter escritor;
        try {
            archivo = new File(pideNombreDeArchivo());
            escritor = new FileWriter(archivo);
            try (BufferedWriter bw = new BufferedWriter(escritor);
		 PrintWriter salida = new PrintWriter(bw)) {		
                salida.write(texto + "\n");
            }
        } catch (IOException e) {
            System.out.println("Error:" + e.getMessage());
        } //Fin del try.	
    } //Fin del método creaTxt.

    /**
     * Pide nombre del archivo al usuario.
     *
     * @return nombre, del archivo propuesto.
     */
    public String pideNombreDeArchivo(){

	System.out.println("¿Con qué nombre quieres que se guarde el archivo con los datos del empaquetamiento?\n");
	Scanner sc = new Scanner (System.in);
	String nombre = sc.next();
	return nombre;
    } //Fin de pideNombreDeArchivo.
	
} //Fin de la clase ServidorAuxiliar.

